# Teste Cuponeria

### Instruções
``` 
1 - Crie um ambiente laravel em qualquer versão >= 8 usando docker.

2 - Crie um endpoint que possa receber os payloads 1, 2 e 3
    2.1 - Esse endpoint deve gravar os dados do payload em um tabela partner_sells
    2.2 - Os dados que devem ser gravados são:
        external_id
        amount
        comission_amount
        payload
        status (APPROVED, PENDING, DENIED)
        currency
        date
        
3 - No readme do projeto, adicione uma breve explicação sobre o porquê de ter desenvolvido da forma que você desenvolveu.(255 caractéres no máximo)
        
4 - Ao finalizar, crie um repositório git que possa ser compartilhado. (O serviço git que será utilizado fica a seu critério)
```

### O que será avaliado
```
1 - Lógica de resolução
2 - Funcionalidade do código
3 - Escalabilidade do código
4 - Utilização de boas práticas de desenvolvimento.
```

###  Boa sorte!